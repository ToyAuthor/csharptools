﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetClient
{
	public partial class Form1 : Form
	{
		private Pvs.Utils.Network.Client _client;

		public Form1()
		{
			InitializeComponent();

			_client = new Pvs.Utils.Network.Client("PvsServer","test");

			Pvs.Utils.Debug.SetPrinter((string str)=>Print(str));
		}
		
		public void Print( string str )
		{
			this.Invoke( (Action)(() =>
			{
				richTextBox1.AppendText( str + "\r" );
				richTextBox1.Update();
			}) );
		}

		private void Form1_FormClosing( object sender, FormClosingEventArgs e )
		{
			_client.Close();
		}

		// 嘗試連線
		private void button1_Click( object sender, EventArgs e )
		{
			_client.Connect();
		}

		// 送出資訊
		private void button2_Click( object sender, EventArgs e )
		{
			_client.Post( textBox1.Text );
		}

		// 取得資訊
		private void button3_Click( object sender, EventArgs e )
		{
			string str;

			if ( _client.Peek(out str) )
			{
				textBox1.Text = str;
			}
		}
	}
}
