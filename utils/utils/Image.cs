﻿//using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;

namespace Pvs
{
	namespace Utils
	{
		public class Image
		{
			// ARGB -> 4
			// RGB -> 3
			static private int GetPixelSize( Bitmap image )
			{
				// 因為傳出來的是 bit 數目而不是 byte
				// 所以要除以8
				return (System.Drawing.Image.GetPixelFormatSize( image.PixelFormat )) >> 3;
			}

			static public BitmapData LockImage( Bitmap image )
			{
				return image.LockBits( new Rectangle( 0, 0, image.Width, image.Height ), ImageLockMode.ReadWrite, image.PixelFormat );
			}

			//-------------------------------------------------------------------------

			// http://www.ebaomonthly.com/window/photo/lesson/colorList.htm
			static public Color MakeColor(byte red, byte green, byte blue)
			{
				return Color.FromArgb( red, green, blue );
			}

			//-------------------------------------------------------------------------

			// 查一下圖檔可能的副檔名
			static public string DetectFormat( string str )
			{
				var types = new Dictionary<string, string>()
				{
					{ "FFD8", ".jpg" },
					{ "424D", ".bmp" },
					{ "474946", ".gif" },
					{ "89504E470D0A1A0A", ".png" }
				};

				string builtHex = string.Empty;

				using ( var S = System.IO.File.OpenRead( str ) )
				{
					for ( int i = 0 ; i < 8 ; i++ )
					{
						builtHex += S.ReadByte().ToString( "X2" );

						if ( types.ContainsKey( builtHex ) )
						{
							return types[builtHex];
						}
					}
				}

				return string.Empty;
			}

			// 不確定這樣做有沒有意義
			static public void Free( Bitmap image )
			{
				WinAPI.DeleteObject( image.GetHbitmap() );
			}

			// 改一下 size
			// (尚未測試過)
			public static Bitmap Resize(Bitmap srcBitmap, int width, int height)
			{
				var image = (System.Drawing.Image)srcBitmap;

				var destRect = new Rectangle(0, 0, width, height);
				var destImage = new Bitmap(width, height);

				destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

				using (var graphics = Graphics.FromImage(destImage))
				using (var wrapMode = new ImageAttributes())
				{
					graphics.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;
					graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
					graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
					graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
					graphics.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;

					wrapMode.SetWrapMode(System.Drawing.Drawing2D.WrapMode.TileFlipXY);
					graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
				}

				return destImage;
			}

			// 上下顛倒
			static public Bitmap Flip(Bitmap image01)
			{
				int width = image01.Width;
				int height = image01.Height;
				int realWidth = width * GetPixelSize(image01);

				var image02 = new Bitmap(width, height);

				var data01 = LockImage(image01);
				var data02 = LockImage(image02);

				unsafe
				{
					byte* ptr01 = (byte*)data01.Scan0;
					byte* ptr02 = (byte*)data02.Scan0;

					int mapsize = (image02.Height) * (data02.Stride);   // image02 的記憶體真實體積

					int stride = data01.Stride;

					byte* p01 = ptr01;
					byte* p02 = ptr02 + (mapsize - stride);   // 指向最後一行的開頭

					for (int i = 1; i <= height; i++)
					{
						for (int j = 0; j < realWidth; j++)
						{
							p02[j] = p01[j];
						}

						p01 += stride;
						p02 -= stride;
					}
				}

				image01.UnlockBits(data01);
				image02.UnlockBits(data02);

				return image02;
			}

			// destBitmap 的長寬有用處，而內容不重要，因為內容會被覆蓋
			static public Bitmap Zoom(Bitmap sourImage, Bitmap destBitmap, bool aspectRatioFixed = false)
			{
				int destWidth  = destBitmap.Width;
				int destHeight = destBitmap.Height;

				try
				{
					int width = 0, height = 0;

					if (aspectRatioFixed)
					{
						// 如果硬要維持長寬比例可以這樣寫，多的部分會用空白來補
						if ((sourImage.Width * destHeight) > (sourImage.Height * destWidth))
						{
							width = destWidth;
							height = (destWidth * sourImage.Height) / sourImage.Width;
						}
						else
						{
							height = destHeight;
							width = (sourImage.Width * destHeight) / sourImage.Height;
						}
					}
					else
					{
						// 隨著給定長寬而變形
						width = destWidth;
						height = destHeight;
					}

					using (Graphics graph = Graphics.FromImage(destBitmap))
					{
						graph.Clear(Color.Transparent);

						// 設定畫布的描繪品質
						graph.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
						graph.SmoothingMode      = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
						graph.InterpolationMode  = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

						graph.DrawImage( sourImage, new Rectangle((destWidth - width) >> 1, (destHeight - height) >> 1, width, height),
						                 0,
						                 0,
						                 sourImage.Width,
						                 sourImage.Height,
						                 GraphicsUnit.Pixel );
					}
					//sourImage.Dispose();

					return destBitmap;
				}
				catch
				{
					Debug.Oops();
					return sourImage;
				}
			}

			// 依給定長寬來縮放，這個會建立新的記憶體，不該頻繁使用
			static public Bitmap Zoom(Bitmap sourImage, int destWidth, int destHeight, bool aspectRatioFixed = false)
			{
				var newBmp = new Bitmap(destWidth, destHeight);

				Zoom(sourImage, newBmp, aspectRatioFixed);

				return newBmp;
			}

			// 直接給個比例來縮放
			// 因此不會有 aspectRatioFixed 這個選項
			static public Bitmap Zoom(Bitmap image,double rate)
			{
				double width = image.Width;
				double height = image.Height;

				return Zoom(image, System.Convert.ToInt32(width *rate), System.Convert.ToInt32(height *rate), true);
			}

			// 建立一個空的灰階圖，幫忙填調色盤
			static public Bitmap MakeEmptyGrayImage(int width, int height)
			{
				var result = new Bitmap(width, height, PixelFormat.Format8bppIndexed);

				var palette = result.Palette;

				for (int i = 0; i < 256; i++)
				{
					palette.Entries[i] = Color.FromArgb(255, i, i, i);
				}

				result.Palette = palette;

				return result;
			}

			static private void Gray_to_RGB(Bitmap sourImage, Bitmap destImage)
			{
				// 檢查一下
				if ((sourImage.Width != destImage.Width) ||
				    (sourImage.Height != destImage.Height) ||
				    (sourImage.PixelFormat != PixelFormat.Format8bppIndexed) ||
				    (destImage.PixelFormat != PixelFormat.Format24bppRgb))
				{
					throw Debug.Exception();
				}

				var data01 = LockImage(sourImage);
				var data02 = LockImage(destImage);

				unsafe
				{
					int width  = destImage.Width;
					int height = destImage.Height;

					byte* ptr01 = (byte*)data01.Scan0;
					byte* ptr02 = (byte*)data02.Scan0;

					int padding01 = data01.Stride - (data01.Width);
					int padding02 = data02.Stride - (data02.Width * 3);

					byte* p01 = ptr01;
					byte* p02 = ptr02;

					for (int i = 1; i <= height; i++)
					{
						for (int j = 0; j < width; j++)
						{
							p02[0] = *p01;
							p02[1] = *p01;
							p02[2] = *p01;

							p01 += 1;
							p02 += 3;
						}

						p01 += padding01;
						p02 += padding02;
					}
				}

				sourImage.UnlockBits(data01);
				destImage.UnlockBits(data02);
			}

			static private void RGB_to_Gray(Bitmap sourImage, Bitmap destImage)
			{
				// 檢查一下
				if ((sourImage.Width != destImage.Width) ||
				    (sourImage.Height != destImage.Height) ||
				    (sourImage.PixelFormat != PixelFormat.Format24bppRgb) ||
				    (destImage.PixelFormat != PixelFormat.Format8bppIndexed))
				{
					throw Debug.Exception();
				}

				var data01 = LockImage(sourImage);
				var data02 = LockImage(destImage);

				unsafe
				{
					int width  = destImage.Width;
					int height = destImage.Height;

					byte* ptr01 = (byte*)data01.Scan0;
					byte* ptr02 = (byte*)data02.Scan0;

					int padding01 = data01.Stride - (data01.Width * 3);
					int padding02 = data02.Stride - (data02.Width);

					byte* p01 = ptr01;
					byte* p02 = ptr02;

					for (int i = 1; i <= height; i++)
					{
						for (int j = 0; j < width; j++)
						{
							float pixel1 = p01[0];
							float pixel2 = p01[1];
							float pixel3 = p01[2];

							*p02 = (byte)System.Math.Ceiling((pixel2 + pixel3 + pixel1) / 3.0);

							p01 += 3;
							p02 += 1;
						}

						p01 += padding01;
						p02 += padding02;
					}
				}

				sourImage.UnlockBits(data01);
				destImage.UnlockBits(data02);
			}

			static private void ARGB_to_Gray(Bitmap sourImage, Bitmap destImage)
			{
				// 檢查一下
				if ((sourImage.Width != destImage.Width) ||
				    (sourImage.Height != destImage.Height) ||
				    (sourImage.PixelFormat != PixelFormat.Format32bppArgb) ||
				    (destImage.PixelFormat != PixelFormat.Format8bppIndexed))
				{
					throw Debug.Exception();
				}

				var data01 = LockImage(sourImage);
				var data02 = LockImage(destImage);

				unsafe
				{
					int width  = destImage.Width;
					int height = destImage.Height;

					byte* ptr01 = (byte*)data01.Scan0;
					byte* ptr02 = (byte*)data02.Scan0;

					int padding01 = data01.Stride - (data01.Width * 4);
					int padding02 = data02.Stride - (data02.Width);

					byte* p01 = ptr01;
					byte* p02 = ptr02;

					for (int i = 1; i <= height; i++)
					{
						for (int j = 0; j < width; j++)
						{
							float pixel1 = p01[0];
							float pixel2 = p01[1];
							float pixel3 = p01[2];
							//float pixel4 = p01[3];

							*p02 = (byte)System.Math.Ceiling((pixel2 + pixel3 + pixel1) / 3.0);

							p01 += 4;
							p02 += 1;
						}

						p01 += padding01;
						p02 += padding02;
					}
				}

				sourImage.UnlockBits(data01);
				destImage.UnlockBits(data02);
			}

			static private void ARGB_to_RGB(Bitmap sourImage, Bitmap destImage)
			{
				// 檢查一下
				if ((sourImage.Width != destImage.Width) ||
				    (sourImage.Height != destImage.Height) ||
				    (sourImage.PixelFormat != PixelFormat.Format32bppArgb) ||
				    (destImage.PixelFormat != PixelFormat.Format24bppRgb))
				{
					throw Debug.Exception();
				}

				var data01 = LockImage(sourImage);
				var data02 = LockImage(destImage);

				unsafe
				{
					int width  = destImage.Width;
					int height = destImage.Height;

					byte* ptr01 = (byte*)data01.Scan0;
					byte* ptr02 = (byte*)data02.Scan0;

					int padding01 = data01.Stride - (data01.Width * 4);
					int padding02 = data02.Stride - (data02.Width * 3);

					byte* p01 = ptr01;
					byte* p02 = ptr02;

					for (int i = 1; i <= height; i++)
					{
						for (int j = 0; j < width; j++)
						{
							p02[0] = p01[0];
							p02[1] = p01[1];
							p02[2] = p01[2];

							p01 += 4;
							p02 += 3;
						}

						p01 += padding01;
						p02 += padding02;
					}
				}

				sourImage.UnlockBits(data01);
				destImage.UnlockBits(data02);
			}

			// 轉成 1 pixel 佔 1byte 的灰階圖
			// 目前只完成 ARGB 轉灰階
			// MakeEmptyGrayImage
			static public Bitmap ToGray(Bitmap sourImage, Bitmap buffer)
			{
				if(buffer.PixelFormat!= PixelFormat.Format8bppIndexed)
				{
					// 連 buffer 格式都給錯，一定要抗議
					throw Debug.Exception();
				}

				// 長寬不一樣的話就只能回傳全新 bitmap 了，無法回收 buffer 來重複使用
				if ((sourImage.Width == buffer.Width) &&
				    (sourImage.Height == buffer.Height))
				{
					// 如果本身就是灰階的話，直接送出去
					if (sourImage.PixelFormat == PixelFormat.Format8bppIndexed)
					{
						return sourImage;
					}
					else if (sourImage.PixelFormat == PixelFormat.Format32bppArgb)
					{
						ARGB_to_Gray(sourImage, buffer);
					}
					else if (sourImage.PixelFormat == PixelFormat.Format24bppRgb)
					{
						RGB_to_Gray(sourImage, buffer);
					}
					else
					{
						// 還無法支援這格式喔
						throw Debug.Exception();
					}
				}
				else
				{
					// 回傳全新 bitmap
					return ToGray(sourImage);
				}

				return buffer;
			}

			// 將圖片轉成灰階圖
			static public Bitmap ToGray(Bitmap sourImage)
			{
				// 如果本身就是灰階的話，直接送出去
				if (sourImage.PixelFormat == PixelFormat.Format8bppIndexed)
				{
					return sourImage;
				}

				return ToGray(sourImage, MakeEmptyGrayImage(sourImage.Width, sourImage.Height));
			}

			static public Bitmap ToRGB(Bitmap sourImage, Bitmap buffer)
			{
				if(buffer.PixelFormat!= PixelFormat.Format24bppRgb)
				{
					// 連 buffer 格式都給錯，一定要抗議
					throw Debug.Exception();
				}

				// 長寬不一樣的話就只能回傳全新 bitmap 了，無法回收 buffer 來重複使用
				if ((sourImage.Width == buffer.Width) &&
				    (sourImage.Height == buffer.Height))
				{
					// 如果本身就是RGB的話，直接送出去
					if (sourImage.PixelFormat == PixelFormat.Format24bppRgb)
					{
						return sourImage;
					}
					else if (sourImage.PixelFormat == PixelFormat.Format32bppArgb)
					{
						ARGB_to_RGB(sourImage, buffer);
					}
					else if (sourImage.PixelFormat == PixelFormat.Format8bppIndexed)
					{
						Gray_to_RGB(sourImage, buffer);
					}
					else
					{
						// 還無法支援這格式喔
						throw Debug.Exception();
					}
				}
				else
				{
					// 回傳全新 bitmap
					return ToRGB(sourImage);
				}

				return buffer;
			}

			// 將圖片轉成 RGB 彩圖
			static public Bitmap ToRGB(Bitmap sourImage)
			{
				return ToRGB(sourImage, new Bitmap(sourImage.Width, sourImage.Height, PixelFormat.Format24bppRgb));
			}

			//-------------------------------------------------------------------------

			// 完全還沒開始，也許永遠不會實作了，因為內建繪圖機制已經堪用
			// 時間一趕就沒機會做這個了
			static public void DrawLine_Gray( Bitmap image, float x1, float y1, float x2, float y2, Color color, float lineWidth )
			{
				if ( image.PixelFormat != PixelFormat.Format8bppIndexed )
				{
					Debug.Oops();
					return;
				}

				var data = LockImage( image );

				unsafe
				{
					int width = image.Width;
					int height = image.Height;

					byte* ptr = (byte*)data.Scan0;

					int padding = data.Stride - image.Width;

					//for ( int i = 1 ; i <= height ; i++ )
					//{
					//	for ( int j = 0 ; j < width ; j++ )
					//	{
					//		if (j==50)
					//		{
					//			ptr[0] = 0;
					//		}

					//		ptr += 1;
					//	}

					//	i += padding;
					//	ptr += padding;
					//}
				}

				image.UnlockBits( data );
			}

			static public void DrawLine( Bitmap image, float x1, float y1, float x2, float y2, Color color, float lineWidth=1 )
			{
				if ( image.PixelFormat == PixelFormat.Format8bppIndexed )
				{
					DrawLine_Gray( image, x1, y1, x2, y2, color, lineWidth );
				}
				else
				{
					Debug.Oops();
				}
			}

			//-------------------------------------------------------------------------

			private Bitmap _image;

			public Image(Bitmap image)
			{
				_image = new Bitmap(image);
			}

			public Bitmap ReadImage()
			{
				return _image;
			}

			public Bitmap CloneBitmap()
			{
				return new Bitmap(_image);
			}

			public void Flip()
			{
				_image = Flip(_image);
			}

			public void Zoom(double rate)
			{
				_image = Zoom(_image, rate);
			}
		}
	}
}
