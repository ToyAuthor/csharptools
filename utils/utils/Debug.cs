﻿using SysCompiler = System.Runtime.CompilerServices;

namespace Pvs
{
	namespace Utils
	{
		public class Debug
		{
			// 預設是給一個不做事的函式
			static private System.Action<string> _printer = ( string str ) => { };

			static public void SetPrinter( System.Action<string> act )
			{
				_printer = act;
			}

			// 可以當成公共輸出管道
			static public void Print(string str)
			{
				_printer(str);
			}

			// 印出呼叫 Oops() 的地點
			static public void Oops( [SysCompiler.CallerLineNumber] int lineNumber = 0, [SysCompiler.CallerFilePath] string filename = null )//, [CallerMemberName] string caller = null)
			{
				_printer( filename + ":" + lineNumber );
			}

			// 還是別把 throw 這個字眼包裝進來好了
			static public System.Exception Exception()
			{
				return new System.Exception();
			}
		}
	}
}
