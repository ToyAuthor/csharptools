﻿using System;
using System.Runtime.InteropServices;

namespace Pvs
{
	namespace Utils
	{
		public class WinAPI
		{
			public const int WM_SYSCOMMAND = 0x112;
			public const uint MF_BYPOSITION = 0x400;

			public const int GWL_STYLE = -16;
			public const int WS_VISIBLE = 0x10000000;
			public const int WS_CHILD = 0x40000000;

			[DllImport( "user32.dll" )]
			static public extern IntPtr GetSystemMenu( IntPtr hWnd, bool bRevert );

			[DllImport( "user32.dll" )]
			static public extern bool InsertMenu( IntPtr hMenu, uint wPosition, uint wFlags, uint wIDNewItem, string lpNewItem );

			// 用來釋放 Bitmap 的記憶體資源，應該用不到才對
			[DllImport( "gdi32.dll" )]
			static public extern bool DeleteObject( IntPtr hObject );

			[DllImport( "user32.dll", SetLastError = true )]
			static public extern int SetParent( IntPtr hWndChild, IntPtr hWndNewParent );

			[DllImport( "user32.dll", EntryPoint = "SetWindowLong" )]
			static public extern int SetWindowLong( IntPtr hWnd, int nIndex, int dwNewLong );

			[DllImport( "user32.dll" )]
			static public extern bool MoveWindow( IntPtr hWnd, int x, int y, int nWidth, int nHeight, bool BRePaint );

			[DllImport( "user32.dll", EntryPoint = "DestroyWindow" )]
			static public extern bool DestroyWindow( IntPtr hWnd );
		}
	}
}
