﻿
namespace Pvs
{
	namespace Utils
	{
		// 架構在 Pipe 之上，內含 thread 處理
		public class Network
		{
			// 感覺外型很像 Mailbox
			// 只是內建連線功能
			public class Client : System.IDisposable
			{
				private bool _isConnected = false;

				// 因為雙向溝通需要兩條 Pipe
				private bool _isRunningSender = true;
				private bool _isRunningReceiver = true;
				private Pipe.NamedClient _clientSender;
				private Pipe.NamedClient _clientReceiver;
				private readonly System.Threading.Thread _threadSender;
				private readonly System.Threading.Thread _threadReceiver;
				private Threading.Mailbox _mailboxSender;
				private Threading.Mailbox _mailboxReceiver;

				private string _bufferReceiver;     // 做為接收字串

				public Client( string serverPipeName, string identity, string serverName = "." )
				{
					_mailboxSender   = new Threading.Mailbox();
					_mailboxReceiver = new Threading.Mailbox();

					_clientSender   = new Pipe.NamedClient( serverPipeName+"_r", identity, serverName );
					_clientReceiver = new Pipe.NamedClient( serverPipeName, identity, serverName );

					_threadSender   = new System.Threading.Thread( MainLoopSender );
					_threadReceiver = new System.Threading.Thread( MainLoopReceiver );

					_threadSender.Start();
					_threadReceiver.Start();
				}

				~Client()
				{
					Close();
				}

				private void MainLoopSender( object pak )
				{
					System.Action act=null;

					while ( _isRunningSender )
					{
						if ( _mailboxSender.Read( ref act )) act();
					}

					_mailboxSender.Cancel();
				}

				private void MainLoopReceiver( object pak )
				{
					{
						System.Action act = null;
						_mailboxReceiver.Read(ref act);    // 單純等著被啟動而已
						act();
					}

					while ( _isRunningReceiver )
					{
						string str = _clientReceiver.Read();

						if ( str=="__goodbye")  // 這是主動斷線時，對方的回應，無需回復
						{
							break;
						}

						_clientReceiver.Send( str );   // 回覆一樣的字串來告知 Server 已成功收到

						if ( str=="__disconnect" )
						{
							_isConnected = false;
							_clientSender.Send("__goodbye");
						}

						// 透過這寫法讓閉包幫忙協助攜帶字串
						_mailboxReceiver.Post(()=> { _bufferReceiver = str; } );

						if ( str=="__disconnect" )
						{
							break;
						}
					}

					_mailboxSender.Cancel();
					_mailboxReceiver.Cancel();
					_clientReceiver.Close();
					_clientSender.Close();
				}

				public void Connect()
				{
					_mailboxSender.Post( () => _clientSender.Connect() );
					_mailboxReceiver.Post( () => _clientReceiver.Connect() );

					_isConnected = true;
				}

				public void Post( string str )
				{
					if ( _isConnected==false ) return;

					_mailboxSender.Post( () =>
					{
						_clientSender.Send( str );

						if ( str != _clientSender.Read() )   // 檢查對方的回覆
						{
							Debug.Oops();
						}

						if ( str == "__disconnect" )
						{
							_isRunningSender = false;
						}
					} );
				}

				public bool Peek(out string output)
				{
					System.Action act = null;

					if ( _mailboxReceiver.Peek( ref act ) )
					{
						act();
						output = _bufferReceiver;

						return true;
					}

					output = "";

					return false;
				}

				public bool Read( out string output )
				{
					System.Action act = null;

					if ( _mailboxReceiver.Read( ref act ) )
					{
						act();
						output = _bufferReceiver;

						return true;
					}

					output = "";

					return false;
				}

				private void ShutDown()
				{
					_isRunningSender = false;
					_isRunningReceiver = false;
				}

				public void Close()
				{
					if ( _isConnected )
					{
						Post("__disconnect");   // 告知斷線消息
						_isConnected = false;
					}

					ShutDown();

					_threadSender.Join();
					_threadReceiver.Join();
				}

				public void Dispose()
				{
					Close();
				}
			}

			// 目前實作只能接待一個 server
			// 未來再擴充成複數 server
			public class Server : System.IDisposable
			{
				private bool _isConnected = false;

				// 因為雙向溝通需要兩條 Pipe
				private bool _isRunningSender = true;
				private bool _isRunningReceiver = true;
				private Pipe.PoorNamedServer _serverSender;
				private Pipe.PoorNamedServer _serverReceiver;
				private readonly System.Threading.Thread _threadSender;
				private readonly System.Threading.Thread _threadReceiver;
				private Threading.Mailbox _mailboxSender;
				private Threading.Mailbox _mailboxReceiver;

				private string _bufferReceiver;

				public Server( string serverPipeName, string identity )
				{
					_serverSender   = new Pipe.PoorNamedServer( serverPipeName, identity );
					_serverReceiver = new Pipe.PoorNamedServer( serverPipeName+"_r", identity );

					_mailboxSender = new Threading.Mailbox();
					_mailboxReceiver = new Threading.Mailbox();

					_threadSender = new System.Threading.Thread( MainLoopSender );
					_threadReceiver = new System.Threading.Thread( MainLoopReceiver );

					_threadSender.Start();
					_threadReceiver.Start();
				}

				~Server()
				{
					Close();
				}

				private void MainLoopSender( object pak )
				{
					System.Action act = null;

					while ( _isRunningSender )
					{
						if ( _mailboxSender.Read( ref act ) ) act();
					}
				}

				private void MainLoopReceiver( object pak )
				{
					{
						System.Action act = null;
						_mailboxReceiver.Read( ref act );    // 單純等著被啟動而已
						act();
					}

					while ( _isRunningReceiver )
					{
						string str = _serverReceiver.Read();

						if ( str=="__goodbye")
						{
							break;
						}

						_serverReceiver.Send( str );   // 回覆一樣的字串來告知 Server 已成功收到

						// 是對方要求斷線的
						if ( str=="__disconnect" )
						{
							_isConnected = false;
							_serverSender.Send("__goodbye");
						}

						// 透過這寫法讓閉包幫忙協助攜帶字串
						_mailboxReceiver.Post( () => { _bufferReceiver = str; } );

						if ( str=="__disconnect" )
						{
							break;
						}
					}

					_mailboxSender.Cancel();
					_mailboxReceiver.Cancel();
					_serverReceiver.Close();
					_serverSender.Close();
				}

				public void WaitForConnection()
				{
					_mailboxSender.Post( () => _serverSender.WaitForConnection() );
					_mailboxReceiver.Post( () => _serverReceiver.WaitForConnection() );

					_isConnected = true;
				}

				public void Post( string str )
				{
					if ( _isConnected==false ) return;

					_mailboxSender.Post( () =>
					{
						_serverSender.Send( str );

						if ( str != _serverSender.Read() )   // 檢查對方的回覆
						{
							Debug.Oops();
						}

						if ( str == "__disconnect" )
						{
							_isRunningSender = false;
						}
					} );
				}

				public bool Peek( out string output )
				{
					System.Action act = null;

					if ( _mailboxReceiver.Peek( ref act ) )
					{
						act();
						output = _bufferReceiver;

						return true;
					}

					output = "";

					return false;
				}

				public bool Read( out string output )
				{
					System.Action act = null;

					if ( _mailboxReceiver.Read( ref act ) )
					{
						act();
						output = _bufferReceiver;

						return true;
					}

					output = "";

					return false;
				}

				private void ShutDown()
				{
					_isRunningSender = false;
					_isRunningReceiver = false;
				}

				public void Close()
				{
					if ( _isConnected )
					{
						Post("__disconnect");   // 告知斷線消息
						_isConnected = false;
					}

					ShutDown();

					_threadSender.Join();
					_threadReceiver.Join();
				}

				public void Dispose()
				{
					Close();
				}
			}
		}
	}
}
