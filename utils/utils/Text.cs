﻿using System;

namespace Pvs
{
	namespace Utils
	{
		/*
		 * 用來製造文字檔
		 * 生出來的文字檔格式是 UTF-8 without BOM
		 * 非常好!
		 */
		public class Text : IDisposable
		{
			private bool _isOpen = false;
			private System.IO.StreamWriter _writer;

			public Text()
			{
				;
			}

			// 好像不是很管用
			// 放任他自行清理的話會在 _writer.Dispose() 這行出問題
			// 還是用 using statement 比較保險
			~Text()
			{
				Close();
			}

			public Text(string filename)
			{
				this.Open(filename);
			}

			public void Open(string filename)
			{
				if (_isOpen)
				{
					Close();
				}

				// 這設定是開啟已存在專案時不覆蓋，而是接著寫下去
				_writer = new System.IO.StreamWriter(filename, true);
				_isOpen = true;
			}

			public void Close()
			{
				if (_isOpen)
				{
					_writer.Dispose();
					_writer = null;
					_isOpen = false;
				}
			}

			// 有繼承 IDisposable 就能實作 using statement 的效果
			public void Dispose()
			{
				Close();
			}

			public void Print(string str)
			{
				if (_isOpen)
					_writer.WriteLine(str);
			}
		}
	}
}
