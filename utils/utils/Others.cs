﻿using System.Windows.Forms;

namespace Pvs
{
	namespace Utils
	{
		// 還不確定該放哪裡的功能就先擺這裡
		public class Others
		{
			// 設定右鍵能出現什麼
			static public ToolStripMenuItem MakeMenuItem( string title, MouseEventHandler act, bool isMarked = false )
			{
				var item = new ToolStripMenuItem();

				item.Text = title;
				item.MouseUp += act;

				// 先預留這招
				if ( isMarked )
				{
					item.Checked = true;
				}

				return item;
			}

			// 用來裝 sub menu 的版本
			static public ToolStripMenuItem MakeMenuItem( string title, params ToolStripMenuItem[] values )
			{
				var item = new ToolStripMenuItem();

				item.Text = title;

				for ( int i = 0 ; i < values.Length ; i++ )
				{
					item.DropDownItems.Add( values[i] );
				}

				return item;
			}
		}
	}
}
