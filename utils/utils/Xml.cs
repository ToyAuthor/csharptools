﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Xml;

namespace Pvs
{
	namespace Utils
	{
		/*
		 * 這個先別使用
		 * 設計上還沒什麼頭緒
		 * XmlDocument 本身真的包得很好了
		 * 實在不大需要其他包裝了
		 * 能做的大概就是每個專案都自行客製化一個 XML 溝通物件
		 */
		class Xml
		{
			private XmlDocument _doc = new XmlDocument();   // 這東西支援 UTF-8
			private string _fileName = "setting.xml";

			public Xml()
			{
				Init();
			}

			public Xml( string name )
			{
				_fileName = name;
				Init();
			}

			~Xml()
			{
				// 啥都不管，覆蓋過去就是了
				Save();
			}

			public void Save()
			{
				_doc.Save( _fileName );
			}

			private void Init()
			{
				//if (System.IO.Directory.Exists("setting.xml"))   //檔案跟資料夾要分開來測試
				if ( System.IO.File.Exists( _fileName ) )
				{
					// 讀取舊xml
					_doc.Load( _fileName );
				}
				else
				{
					// 建立新的xml
					var root = _doc.CreateElement( _fileName );
					_doc.AppendChild( root );

					var swith = _doc.CreateElement( "switch" );
					swith.SetAttribute( "removeable", "true" );
					swith.SetAttribute( "test", "false" );
					root.AppendChild( swith );
				}
			}
		}
	}
}
