﻿using StdRegex = System.Text.RegularExpressions;
using StdGeneric = System.Collections.Generic;

namespace Pvs
{
	namespace Utils
	{
		public class Regex
		{
			private StdRegex.Regex _core;

			public Regex( string pattern )
			{
				_core = new StdRegex.Regex( pattern );
			}

			public StdGeneric.List<string> GetMatches( string input )
			{
				var list = new StdGeneric.List<string>();

				foreach ( StdRegex.Match match in _core.Matches( input ) )
				{
					list.Add( match.Value );
				}

				return list;
			}
		}
	}
}
