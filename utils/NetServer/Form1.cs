﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetServer
{
	public partial class Form1 : Form
	{
		private Pvs.Utils.Network.Server _server;

		public Form1()
		{
			InitializeComponent();

			_server = new Pvs.Utils.Network.Server( "PvsServer", "test" );

			Pvs.Utils.Debug.SetPrinter( ( string str ) => Print( str ) );
		}

		public void Print( string str )
		{
			this.Invoke( (Action)(() =>
			{
				richTextBox1.AppendText( str + "\r" );
				richTextBox1.Update();
			}) );
		}

		private void Form1_FormClosing( object sender, FormClosingEventArgs e )
		{
			_server.Close();
		}

		// 等待連線
		private void button1_Click( object sender, EventArgs e )
		{
			_server.WaitForConnection();
		}

		// 送出資訊
		private void button2_Click( object sender, EventArgs e )
		{
			_server.Post(textBox1.Text);
		}

		// 取得資訊
		private void button3_Click( object sender, EventArgs e )
		{
			string str;

			if ( _server.Peek(out str) )
			{
				textBox1.Text = str;
			}
		}
	}
}
