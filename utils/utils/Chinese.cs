﻿
namespace Pvs
{
	namespace Utils
	{
		public class Chinese
		{
			// 繁體轉成簡體
			static public string ToSimplified(string str)
			{
				// 簡體中文 (GB2312) 系統的 LocaleID (LCID) 為 2052
				return Microsoft.VisualBasic.Strings.StrConv(str, Microsoft.VisualBasic.VbStrConv.SimplifiedChinese, 2052);
			}

			// 簡體轉成繁體
			static public string ToTraditional(string str)
			{
				// 繁體中文 (Big5) 系統的 LocaleID (LCID) 為 1028
				return Microsoft.VisualBasic.Strings.StrConv(str, Microsoft.VisualBasic.VbStrConv.TraditionalChinese, 1028);
			}
		}
	}
}
