﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using Tools = Pvs.Utils;

namespace DemoCS002
{
	public partial class Form1 : Form
	{
		private World _my001;
		private World _my002;
		private World _my003;

		public Form1()
		{
			InitializeComponent();

			Tools.Debug.SetPrinter((string str)=>Print(str));

			InitRichTextMenu();

			_my001 = new World();
			_my002 = new World();
			_my003 = new World();
		}

		private void Print(string str)
		{
			this.BeginInvoke((Action)(()=>
			{
				richTextBox1.AppendText( str + "\r" );
				richTextBox1.Update();
			} ));
		}

		private void InitRichTextMenu()
		{
			var menu = new ContextMenuStrip();

			menu.Items.Add( Tools.Others.MakeMenuItem( "清空內容", ( object sender, MouseEventArgs e ) => richTextBox1.Clear() ) );

			richTextBox1.ContextMenuStrip = menu;
		}

		private string TestName(string filename )
		{
			string tick = Tools.Date.GetDateAndTime();

			var regex01 = new Tools.Regex( @"\.[^\.]+$" );   // 用來找副檔名(含"."號)

			var list01 = regex01.GetMatches( filename );

			if ( list01.Count == 0 )
			{
				// 表示沒有副檔名
				filename += tick;
			}
			else
			{
				// 用來找檔名，"."號以後的都不要
				var regex02 = new Tools.Regex( @".*(?=\.[^\.]+$)" );

				var list02 = regex02.GetMatches( filename );

				if ( list02.Count == 0 )
				{
					// 表示抓不到檔名
					Tools.Debug.Oops();
				}
				else
				{
					return list02[0] + "_" + tick + list01[0];
				}
			}

			return filename;
		}

		private void button1_Click( object sender, EventArgs e )
		{
			Tools.Debug.Print( "哈囉" );
			_my001.Post( () => Print( "說點甚麼1" ) );
			_my003.Post( () => Print( "說點甚麼2" ) );
			_my002.Post( () => Print( "說點甚麼3" ) );

			//Print( TestName( textBox1.Text ) );
		}

		private void Form1_FormClosing( object sender, FormClosingEventArgs e )
		{
			_my001.Join();
			_my002.Join();
			_my003.Join();
		}
	}
}
