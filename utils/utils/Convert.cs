﻿
namespace Pvs
{
	namespace Utils
	{
		public class Convert
		{
			public static float ToFloat(string str)
			{
				return System.Convert.ToSingle( str );
			}

			// 這大概用不到
			public static string ToString( float num )
			{
				return num.ToString();
			}
		}
	}
}
