﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using Tools = Pvs.Utils;

namespace DemoCS002
{
	class World
	{
		private readonly Thread _thread;
		private readonly Tools.Threading.Mailbox _mailbox = new Tools.Threading.Mailbox();
		private bool _isRunning = true;

		private void MainLoop( object obj )
		{
			World root = (World)obj;
			Action act = null;

			while ( _isRunning )
			{
				if ( _mailbox.Peek( ref act ))
					act();
				//root.SaySomeThing();
			}
		}

		public World()
		{
			_thread = new Thread( MainLoop );
			_thread.Start( this );
		}

		public void Post( Action act )
		{
			_mailbox.Post(act);
		}

		public void ShutDown()
		{
			_isRunning = false;
			_mailbox.Cancel();
		}

		public void SaySomeThing()
		{
			Tools.Debug.Print( "我在這裡" );
		}

		public void Join()
		{
			ShutDown();     // 不關閉的話，Join 就沒意義了
			_thread.Join();
		}
	}
}
