﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PipeServer
{
	public partial class Form1 : Form
	{
		private System.Threading.Thread _thread;

		public Form1()
		{
			InitializeComponent();
			InitRichTextMenu();
			Pvs.Utils.Debug.SetPrinter( ( string str ) => Print( str ) );
		}

		private static void ServerThread( object data )
		{
			// 替 server 取個名字
			var server = new Pvs.Utils.Pipe.PoorNamedServer( "PvsServer", "I am the one true server!" );

			//Pvs.Utils.Debug.Print( "Wait for a client to connect" );

			// Wait for a client to connect
			server.WaitForConnection();

			try
			{
				// server 和 client 之間一進一出
				while ( true )
				{
					string cmdstr = server.Read();

					if ( cmdstr == "Disconnect" )
					{
						server.Send( "Disconnect!" );
						System.Threading.Thread.Sleep( 100 );
						break;
					}
					else
					{
						Pvs.Utils.Debug.Print( "丟回去的訊息"+ cmdstr );
						cmdstr = "(Ack)" + cmdstr;
						server.Send( cmdstr );
					}
				}
			}
			catch ( System.IO.IOException e )
			{
				Pvs.Utils.Debug.Print(string.Format( "ERROR: {0}", e.Message ));
			}

			server.Close();
		}

		public void Print( string str )
		{
			this.Invoke( (Action)(() =>
			{
				richTextBox1.AppendText( str + "_server\r" );
				richTextBox1.Update();
			}) );
		}

		private void InitRichTextMenu()
		{
			var menu = new ContextMenuStrip();

			menu.Items.Add( Pvs.Utils.Others.MakeMenuItem( "清空內容", ( object sender, MouseEventArgs e ) => richTextBox1.Clear() ) );

			richTextBox1.ContextMenuStrip = menu;
		}
		
		private void button2_Click( object sender, EventArgs e )
		{
			_thread = new System.Threading.Thread( ServerThread );
			_thread.Start();
		}
	}
}
