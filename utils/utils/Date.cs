﻿using System;

namespace Pvs
{
	namespace Utils
	{
		public class Date
		{
			static public string GetDateAndTime()
			{
				// 年 月 日 hour min
				return DateTime.Now.ToString( "yyyy_MM_dd_HH_mm" );
			}
		}
	}
}
