﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Tools = Pvs.Utils;

namespace DemoCS
{
	public partial class Form1 : Form
	{
		private Bitmap _image;  // 當個載體好了
		private Pen _pen;
		private float _x, _y;

		private void Print(string str)
		{
			richTextBox1.AppendText(str + "\r");
			richTextBox1.Update();
		}

		public Form1()
		{
			InitializeComponent();
			Tools.Debug.SetPrinter( ( string str ) => Print( str ) );
			_pen = new Pen(Color.Black, 2);  // 指定畫筆的顏色與粗細
			_image = new Bitmap(pictureBox1.Size.Width, pictureBox1.Size.Height);
			pictureBox1.BackColor = Color.White;
			pictureBox1.Image = _image;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			Print( Tools.Chinese.ToSimplified(textBox1.Text));
		}

		private void button2_Click(object sender, EventArgs e)
		{
			richTextBox1.Clear();

			using (var dev = new Tools.Text("momo.txt"))
			{
				dev.Print("gg");
				dev.Print("tt中文ff");
				dev.Print("繁體字");
				dev.Print( Tools.Chinese.ToSimplified("繁體"));
				dev.Print("gggg");
			}
		}

		private void button3_Click(object sender, EventArgs e)
		{
			if (openFileDialog1.ShowDialog() == DialogResult.OK)
			{
				Print("載入:" + openFileDialog1.FileName);

				// 這樣載入才能維持原本的格式
				_image = new Bitmap(openFileDialog1.FileName);
				pictureBox1.Image = _image;

				//using (var fs = new System.IO.FileStream(openFileDialog1.FileName, System.IO.FileMode.Open))
				//{
				//	_image = new Bitmap(fs);   // 似乎各種格式都會被硬轉成 RGBA 的格式
				//	/*
				//	 * 這看似多此一舉的複製行為其實是為了甩開 FileStream 的鎖定
				//	 * 如果不這麼做就不能第一時間將圖片檔存起來
				//	 * _image.Save() 會出錯
				//	 */
				//	_image = new Bitmap(_image);
				//	pictureBox1.Image = _image;
				//}
			}
		}

		private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
		{
			_x = e.X;
			_y = e.Y;
		}

		private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
		{
			//宣告畫布的來源是bmp圖案物件
			var g = Graphics.FromImage(_image);

			//如果按下滑鼠左鍵時
			if (e.Button == MouseButtons.Left)
			{
				//隨指標移動不斷在畫布上(圖案物件)畫短點直線
				g.DrawLine(_pen, _x, _y, e.X, e.Y);
				//用圖片方塊picDraw來顯示畫布(圖案物件)的內容
				pictureBox1.Image = _image;
				_x = e.X;
				_y = e.Y;
			}
		}

		private void button5_Click( object sender, EventArgs e )
		{
			//Tools.Image.DrawLine( _image );   // 大概以後都不會去實作這個了
			pictureBox1.Image = _image;
		}

		private void button4_Click(object sender, EventArgs e)
		{
			// 這邊的選項是騙人的，格式不會轉
			// 只有像文字檔這種單純只會改副檔名的類型才適用
			saveFileDialog1.Filter = "點陣圖 (*.bmp)|*.bmp|JPEG (*.JPG)|*.JPG|GIF(*.GIF)|*.GIF|All File (*.*)|*.*";

			// 執行saveFileDialog1.ShowDialog()，將圖案物件的內容儲存到指定的檔案內
			if (saveFileDialog1.ShowDialog() == DialogResult.OK)
			{
				Print("儲存到:" + saveFileDialog1.FileName);
				//_image.Save(saveFileDialog1.FileName);

				Print("PixelFormat:" + _image.PixelFormat.ToString());

				var grayOne = Tools.Image.MakeEmptyGrayImage(_image.Width, _image.Height);

				//Tools.Image.Zoom(_image, 0.75).Save(saveFileDialog1.FileName);
				Tools.Image.ToGray(_image, grayOne).Save(saveFileDialog1.FileName);

				//Tools.Image.Flip(_image).Save(saveFileDialog1.FileName);
			}
		}
	}
}
