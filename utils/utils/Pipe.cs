﻿// 內容源自此處
// https://docs.microsoft.com/zh-tw/dotnet/standard/io/how-to-use-anonymous-pipes-for-local-interprocess-communication
// https://docs.microsoft.com/zh-tw/dotnet/standard/io/how-to-use-named-pipes-for-network-interprocess-communication
using StdIO = System.IO;
using StdPip = System.IO.Pipes;

namespace Pvs
{
	namespace Utils
	{
		// Anonymous 模式只能一個 server 一個 client
		// 成雙成對
		// 無法使用網路線
		// 而且只有單向溝通(固定client聽 server講)
		public class Pipe
		{
			public class AnonymousServer : System.IDisposable
			{
				private StdPip.AnonymousPipeServerStream _server;
				private System.Diagnostics.Process _client;
				private StdIO.StreamWriter _writer;
				private readonly string _IDENTITY;       // 就是個暗號，分辨用的

				public AnonymousServer(string targetName, string identity )    // 尋找同資料夾下的同名程式，將會執行它
				{
					_IDENTITY = identity;

					_server = new StdPip.AnonymousPipeServerStream( StdPip.PipeDirection.Out, StdIO.HandleInheritability.Inheritable );

					_client = new System.Diagnostics.Process();
					_client.StartInfo.FileName = targetName;
					_client.StartInfo.Arguments = _server.GetClientHandleAsString();
					_client.StartInfo.UseShellExecute = false;
					_client.Start();

					_server.DisposeLocalCopyOfClientHandle();

					_writer = new StdIO.StreamWriter( _server );
					_writer.AutoFlush = true;
					_writer.WriteLine( _IDENTITY );   // Send a 'sync message' and wait for client to receive it.

					_server.WaitForPipeDrain();
				}

				public void Send(string str)
				{
					if ( Empty() )
					{
						Debug.Oops();
						return;
					}

					_writer.WriteLine( str );
				}

				public bool Empty()
				{
					if ( _server == null ) return true;

					return false;
				}

				public void Close()
				{
					if ( Empty() ) return;

					_writer.Dispose();
					_writer = null;

					_server.Dispose();
					_server = null;

					_client.WaitForExit();
					_client.Close();
				}

				public void Dispose()
				{
					Close();
				}
			}

			public class AnonymousClient : System.IDisposable
			{
				private StdPip.PipeStream _pipeClient;
				private StdIO.StreamReader _reader;
				private readonly string _IDENTITY;       // 就是個暗號，分辨用的

				/*
				 * ID 是 Server 給的，不用管這是什麼
				 * 轉送給 AnonymousPipeClientStream 就對了
				 * static void Main() 改成 static void Main(string[] args) 就可以抓到 ID
				 * args[0] 就是 ID
				 */
				public AnonymousClient( string myID, string identity )
				{
					_IDENTITY = identity;
					_pipeClient = new StdPip.AnonymousPipeClientStream( StdPip.PipeDirection.In, myID );
					_reader = new StdIO.StreamReader( _pipeClient );
				}

				~AnonymousClient()
				{
					Close();
				}

				public void EventLoop()
				{
					if ( Empty() )
					{
						Debug.Oops();
						return;
					}

					string buffer;

					do
					{
						// Wait for sync...
						buffer = _reader.ReadLine();
					}
					while ( !buffer.StartsWith( _IDENTITY ) );

					// Read the server data and echo to the console.
					while ( (buffer = _reader.ReadLine()) != null )
					{
						Debug.Print( "[CLIENT] Echo: " + buffer );
					}
				}

				public bool Empty()
				{
					if ( _pipeClient == null ) return true;

					return false;
				}

				public void Close()
				{
					if ( Empty() ) return;

					_reader.Dispose();
					_reader = null;

					_pipeClient.Dispose();
					_pipeClient = null;
				}

				public void Dispose()
				{
					Close();
				}
			}

			//--------------------------------------------------------------------------------

			// 只能接待一個 client 而已
			public class PoorNamedServer : System.IDisposable
			{
				private StdPip.NamedPipeServerStream _server;
				private StreamString _streamString;
				private readonly string _IDENTITY;       // 就是個暗號，分辨用的
				private bool _isConnected = false;

				// Client 需要尋找 serverPipeName 這個名字
				public PoorNamedServer( string serverPipeName, string identity )
				{
					// 只開一條 thread 給 client
					_server = new StdPip.NamedPipeServerStream( serverPipeName, StdPip.PipeDirection.InOut, 1 );
					_IDENTITY = identity;
				}

				// 會 blocking 直到 client 來訪
				public void WaitForConnection()
				{
					if ( _server == null )
					{
						Debug.Oops();
						return;
					}

					if ( _isConnected==true )
					{
						// 已經有跟某個 client 連線了
						Debug.Oops();
						return;
					}

					_isConnected = true;

					try
					{
						// 就是它在 blocking
						_server.WaitForConnection();
						_streamString = new StreamString( _server );
						_streamString.WriteString( _IDENTITY );  // 給 client 的暗號
					}
					catch ( StdIO.IOException e )
					{
						// 如果連線有問題就會噴在這
						Debug.Print(string.Format( "ERROR: {0}", e.Message ) );
						_isConnected = false;
					}
				}

				public void Send( string str )
				{
					if ( _isConnected == false )
					{
						Debug.Oops();
						return;
					}

					_streamString.WriteString( str );
				}

				public string Read()
				{
					if ( _isConnected == false )
					{
						Debug.Oops();
						return string.Empty;
					}

					return _streamString.ReadString();
				}

				~PoorNamedServer()
				{
					Close();
				}

				public void Close()
				{
					if ( _server != null )
					{
						_isConnected = false;
						_server.Close();
						_server = null;
					}
				}

				public void Dispose()
				{
					Close();
				}
			}

			//----------------------------------------------------------------------------------

			public class NamedClient : System.IDisposable
			{
				private StdPip.NamedPipeClientStream _client;
				private StreamString _streamString;
				private readonly string _IDENTITY;
				private bool _isConnected = false;

				// 程式位於同一台電腦的話，位置填"."就好
				public NamedClient( string serverPipeName, string identity, string serverName="." )
				{
					_IDENTITY = identity;

					_client = new StdPip.NamedPipeClientStream( serverName, serverPipeName,
						StdPip.PipeDirection.InOut,
						StdPip.PipeOptions.None,
						System.Security.Principal.TokenImpersonationLevel.Impersonation );
				}

				public void Connect()
				{
					if ( _client == null )
					{
						Debug.Oops();
						return;
					}

					if ( _isConnected == true )
					{
						Debug.Oops();
						return;
					}

					_client.Connect();

					_streamString = new StreamString( _client );

					string reremsg = _streamString.ReadString();

					if ( reremsg != _IDENTITY )
					{
						Debug.Oops();
						Debug.Print( "印了這個"+reremsg );
						return;
					}

					_isConnected = true;
				}

				public void Send(string str)
				{
					if ( _isConnected == false )
					{
						Debug.Oops();
						return;
					}

					_streamString.WriteString( str );
				}

				public string Read()
				{
					if ( _isConnected == false )
					{
						Debug.Oops();
						return string.Empty;
					}

					return _streamString.ReadString();
				}

				~NamedClient()
				{
					Close();
				}

				public void Close()
				{
					if ( _client != null )
					{
						_isConnected = false;
						_client.Close();
						_client = null;
					}
				}

				public void Dispose()
				{
					Close();
				}
			}

			//----------------------------------------------------------------------------

			// Defines the data protocol for reading and writing strings on our stream
			class StreamString
			{
				private StdIO.Stream _ioStream;
				private System.Text.UnicodeEncoding _streamEncoding;

				public StreamString( StdIO.Stream ioStream )
				{
					_ioStream = ioStream;
					_streamEncoding = new System.Text.UnicodeEncoding();
				}

				public string ReadString()
				{
					int len = 0;

					len = _ioStream.ReadByte() * 256;
					len += _ioStream.ReadByte();
					byte[] inBuffer = new byte[len];
					_ioStream.Read( inBuffer, 0, len );

					return _streamEncoding.GetString( inBuffer );
				}

				public int WriteString( string outString )
				{
					byte[] outBuffer = _streamEncoding.GetBytes( outString );
					int len = outBuffer.Length;

					if ( len > System.UInt16.MaxValue )
					{
						len = (int)System.UInt16.MaxValue;
					}

					_ioStream.WriteByte( (byte)(len / 256) );
					_ioStream.WriteByte( (byte)(len & 255) );
					_ioStream.Write( outBuffer, 0, len );
					_ioStream.Flush();

					return outBuffer.Length + 2;
				}
			}
		}
	}
}
