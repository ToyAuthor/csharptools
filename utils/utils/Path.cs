﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pvs
{
	namespace Utils
	{
		public class Path
		{
			public static void RemoveFile(string filename)
			{
				if ( System.IO.File.Exists( filename ) )
				{
					System.IO.File.Delete( filename );
				}
			}

		}
	}
}
