﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PipeClient
{
	public partial class Form1 : Form
	{
		private Pvs.Utils.Pipe.NamedClient _client;

		public Form1()
		{
			InitializeComponent();

			InitRichTextMenu();
			Pvs.Utils.Debug.SetPrinter((string str)=> Print(str) );

			_client = new Pvs.Utils.Pipe.NamedClient( "PvsServer", "I am the one true server!" );
		}

		private void InitRichTextMenu()
		{
			var menu = new ContextMenuStrip();

			menu.Items.Add( Pvs.Utils.Others.MakeMenuItem( "清空內容", ( object sender, MouseEventArgs e ) => richTextBox1.Clear() ) );

			richTextBox1.ContextMenuStrip = menu;
		}

		public void Print( string str )
		{
			this.Invoke((Action)(()=>
			{
				richTextBox1.AppendText(str+ "_client\r" );
				richTextBox1.Update();
			}));
		}

		private void Form1_FormClosing( object sender, FormClosingEventArgs e )
		{
			_client.Dispose();
		}

		private void button1_Click( object sender, EventArgs e )
		{
			_client.Connect();
		}

		private void button2_Click( object sender, EventArgs e )
		{
			_client.Send( textBox1.Text );
			Print("收到"+_client.Read());
		}
	}
}
